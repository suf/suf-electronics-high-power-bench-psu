$fn=100;
difference()
{
    cube([147,215,2.5]);
    translate([147/2,215/2,0])
        for(i=[-1,1])
            for(j=[-1,1])
                translate([i*67.5,j*101.5,-0.001])
                    cylinder(d=5,h=2.502);
    for(k=[65,150])
        for(i=[0:7])
            for(j=[0:7])
                translate([12+(i*15.625),k+(j*7.125),-0.001])
                    cube([12.625,5.125,2.502]);
    translate([32,18,-0.001])  
        minkowski()
        {
            cube([28,20,1.251]);
            cylinder(d=5,h=1.251);
        }
    translate([41,39.999,-0.001])  
        cube([10,3,1.2]);
    translate([38,13,-0.001])
        cube([4,3.001,1.2]);
    translate([50,13,-0.001])
        cube([4,3.001,1.2]);
    translate([100,33,0])
    {
        translate([-14,0,-0.001])
            cylinder(d=3.5,h=2.502);
        translate([14,0,-0.001])
            cylinder(d=3.5,h=2.502);
        translate([-6,-4.5,1])
            cube([12,9,1.501]);
        translate([-4.5,-2.25,-0.001])
            cube([9,4.5,1.002]);
    }
}